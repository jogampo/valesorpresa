import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValeVirginiaComponent } from './vale-virginia.component';

describe('ValeVirginiaComponent', () => {
  let component: ValeVirginiaComponent;
  let fixture: ComponentFixture<ValeVirginiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValeVirginiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValeVirginiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
