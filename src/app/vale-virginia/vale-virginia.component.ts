import { Component, OnInit } from '@angular/core';
import { HammerModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-vale-virginia',
  templateUrl: './vale-virginia.component.html',
  styleUrls: ['./vale-virginia.component.css']
})
export class ValeVirginiaComponent implements OnInit {

  path: string = "./assets/regalo.png";
  
  constructor() { }

  ngOnInit(): void {
  }

  
  onClickMe() {
    window.location.href = 'https://www.musaelite.com/tratamientos-capilares/';
  }

}
